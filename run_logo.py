#!/usr/bin/env python
import csv
import commands
from tqdm import tqdm
from multiprocessing.pool import ThreadPool

reader = csv.reader(open('bars.csv'))
cmd = []

for line in reader:
    grab_id = line[0]
    mexName = line[1]
    cmd.append("python2 mex_placeholder.py -i ./Bars.jpg -n '"+mexName+"' -o 'imgs/"+grab_id+"' -f Sanomat\ Grab\ Web\ Medium\ Regular.ttf ")

def generate_logos(c):
    #status_code, err_msg = commands.getstatusoutput(c)
    print cmd
    #if status_code != 0:
        #return c, err_msg
    
    return c, None

res = list(tqdm(ThreadPool(20).imap_unordered(generate_logos, cmd), total=(len(cmd))))
for command, error in res:
    if error is not None:
        print("error fetching %r: %s" % (command, error))


#!/usr/bin/python
"""
Generate Mex placeholder with 1-1 or 9-5 aspect ratio.

Usage example:

    ./mex_placeholder.py -f ../Sanomat/SanomatSans-Bold.otf -i ../MEX_Placeholder_95.jpg -n 'Mex Test' -o output.jpg
"""

import re
import argparse
import numpy as np
import piexif
import piexif.helper
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 
from ast import literal_eval as make_tuple


MAX_LINES = 6
MAX_LEN = 100000
# FONT_COLOR = (0, 150, 0)
# Embed this in Exif Tag
ID_TAG = 'Mex Picture Autogen'

def _wrap_text(txt):
    """
    Wrap text such that the mex name is approximately aligned at both ends.
    As far as possible, we want to split into MAX_LINES lines (maximum) so that 
    the text can be large and more visible.
    
    We do not use TextWrap as it cannot vary the width per line, which is limiting.
    
    :param str txt: text string
    :return: wrapped text
    """
    # replace whitespace with space
    txt = re.sub(r"\s+", ' ', txt).strip()
    # possible split locations
    pos = np.array([m.start() for m in re.finditer(' ', txt)])
    desired_pos = float(len(txt)) / MAX_LINES * np.arange(1, MAX_LINES)
    #print(pos)
    #print(desired_pos)
    
    new_txt = list(txt)
    for d in desired_pos:
        lpos = pos[pos <= d]
        rpos = pos[pos >= d]
        if (len(lpos) == 0) and (len(rpos) == 0):
            break
        lval = lpos[-1] if len(lpos) > 0 else -MAX_LEN
        rval = rpos[0] if len(rpos) > 0 else MAX_LEN
        
        # use the split location nearer to desired position
        p = lval if (d - lval) < (rval - d) else rval
        #print(d, lval, rval, p)
        new_txt[p] = '\n'
        
    new_txt = ''.join(new_txt)
    return new_txt
    
def _calc_font_size(txt, bbox_width, bbox_height, font_path):
    """
    Compute the appropriate font size given the bounding box size.
    
    :param str txt: text string
    :param int bbox_width: in pixels
    :param int bbox_height: in pixels
    :param str font_path: full path of ttf font
    :return: font object, optimal font size, text width, text height, line spacing
    """
    MIN_FONT_SIZE = 60 # we expect background image to be at least 500px wide
    MAX_FACTOR = 0.4 # max ratio of text size/image size
    RANGE = 50 # search range
    
    num_splits = txt.count('\n')

    # compute max font size as a ratio of bbox size
    max_font_size = min(int((9 + (MAX_FACTOR * bbox_width)) // 10 * 10),
                        int((9 + (MAX_FACTOR * bbox_height)) // 10 * 10))
    font = ImageFont.truetype(font_path, MIN_FONT_SIZE)
    width, height = font.getsize_multiline(txt)
    
    # compute a nominal scale for the font
    nominal_size = min((9 + (bbox_width // width + 1) * MIN_FONT_SIZE) // 10 * 10, 
                       (9 + (bbox_height // height + 1) * MIN_FONT_SIZE) // 10 * 10)
    nominal_size = min(max_font_size, nominal_size)

    # try in increments around the nominal range
    for font_size in range(min(max_font_size, nominal_size+RANGE), nominal_size-RANGE-5, -5):
        font = ImageFont.truetype(font_path, font_size)
        width, height = font.getsize_multiline(txt)
        
        # adjust height with line spacings (set to 0.35)
        line_spacing = 0.35 * height / (num_splits + 1)
        height = int(height + line_spacing * num_splits)

        #print(max_font_size, nominal_size, font_size, width, bbox_width, height, bbox_height)
        if width < bbox_width and height < bbox_height:
            return font, font_size, width, height, line_spacing
        
    # else just use min font size
    return font, font_size, width, height, line_spacing

def _embed_name(img_fname, mex_name, font_path, font_color, padding_x=0, padding_y=0, target_height=720, bg_random=False):
    if bg_random:
        exif_bytes = None

        color = (np.random.randint(0, 255), np.random.randint(0, 255), np.random.randint(0, 255))
        font_color = (
            np.random.randint(0, 255) ^ color[0], 
            np.random.randint(0, 255) ^ color[1], 
            np.random.randint(0, 255) ^ color[2])
        img = Image.new('RGB', (1280, 720), color)
    else:
        # embed information so as to be able to identify image later on
        exif_dict = piexif.load(img_fname)
        user_comment = piexif.helper.UserComment.dump(ID_TAG)
        exif_dict['Exif'][piexif.ExifIFD.UserComment] = user_comment
        exif_bytes = piexif.dump(exif_dict)

        img = Image.open(img_fname)

    width, height = img.size
    draw = ImageDraw.Draw(img)

    txt = _wrap_text(mex_name)
    font, font_size, w, h, line_spacing = _calc_font_size(txt, width-2*padding_x, height-2*padding_y, font_path)
    #print(font_size)

    draw.multiline_text(((width-w)/2, (height-h)/2), txt, font_color, font=font, align='center', spacing=line_spacing)

    new_height = target_height
    new_width = int(float(width) / height * new_height)
    img = img.resize((new_width, new_height), Image.BILINEAR)

    return img, exif_bytes

def parse_args():
    parser = argparse.ArgumentParser(description='Auto Generate Mex Placeholder Image')
    parser.add_argument('-f', '--font', required=True, type=str, default=None, metavar='FILENAME', help='Font path')
    parser.add_argument('-c', '--color', required=False, type=str, default='0,150,0', metavar='COLOR', help='Font color')
    parser.add_argument('-i', '--image', required=False, type=str, default=None, metavar='FILENAME', help='Background image')
    parser.add_argument('-n', '--name', required=True, type=str, default=None, metavar='STRING', help='Mex name')
    parser.add_argument('-o', '--output', required=True, type=str, default=None, metavar='FILENAME', help='Filename of output image')
    parser.add_argument('-s', '--square', required=False, action='store_true', 
        help='A switch to select padding values based on whether 1-1 or 9-5 hero image is being used. Alternatively, specify the paddings.')
    parser.add_argument('-x', '--paddingx', required=False, type=int, default=None, help='Optionally, specify X padding in pixels')
    parser.add_argument('-y', '--paddingy', required=False, type=int, default=None, help='Optionally, specify Y padding in pixels')
    parser.add_argument('-t', '--height', required=False, type=int, default=720, help='Target image height in pixels')
    parser.add_argument('-r', '--random', required=False, action='store_true', help='Randomize colour background and text color')
    args = parser.parse_args()

    return args

def main(args):
    if args.square:
        # These padding values are based on the 1-1 background picture given by Nirusha
        padding_x = 600 if not args.paddingx else args.paddingx
        padding_y = 560 if not args.paddingy else args.paddingy
    else:
        # padding values for 9-5 background picture
        padding_x = 240 if not args.paddingx else args.paddingx
        padding_y = 170 if not args.paddingy else args.paddingy

    img, exif_bytes = _embed_name(args.image, args.name, args.font, make_tuple(args.color), 
                                padding_x, padding_y, args.height, args.random)
    if args.output:
        if exif_bytes:
            img.save(args.output, exif=exif_bytes, quality=95)
        else:
            img.save(args.output, quality=95)


if __name__ == "__main__":
    args = parse_args()
    main(args) 
    

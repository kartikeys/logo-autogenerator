#!/usr/bin/env python
import csv
import commands
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('name')
args = parser.parse_args()

reader = csv.reader(open(args.name))

for line in reader:
    grab_id = line[0]
    mexName = line[1]
    mexName = mexName.title()
    mexName = mexName.split(' -')[0]
    mexName = mexName.split(' @')[0]
    mexName = mexName.split(' |')[0]
    mexName = mexName.split(' (')[0]
    if len(mexName) > 30:
        print(grab_id)


#!/usr/bin/env python
from multiprocessing.pool import ThreadPool
from time import time as timer
import commands
import os
from tqdm import tqdm

destBucket = "s3://cdn-xm-fileservice/entities/"
imgDir = "imgs"
cmd = []

for filename in os.listdir(imgDir):
    filename = str(os.path.splitext(filename)[0])
    command = "aws s3 cp "+imgDir+"/"+filename+".jpg "+destBucket+filename+"/StoreLogo/v1/"+filename+"_StoreLogo"
    cmd.append(command)

def fetch_url(c):
    status_code, err_msg = commands.getstatusoutput(c)
    if status_code != 0:
        return c, err_msg
    
    return c, None
    
start = timer()
res = list(tqdm(ThreadPool(20).imap_unordered(fetch_url, cmd), total=(len(cmd))))
for msg, error in res:
    if error is not None:
        print("error fetching %r: %s" % (msg, error))

print("Elapsed Time: %s" % (timer() - start))

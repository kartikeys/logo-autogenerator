# Auto Generate Mex Placeholder Image

## Usage

    usage: mex_placeholder.py [-h] -f FILENAME -i FILENAME -n STRING -o FILENAME
                            [-s] [-x PADDINGX] [-y PADDINGY]

    Auto Generate Mex Placeholder Image

    optional arguments:
    -h, --help            show this help message and exit
    -f FILENAME, --font FILENAME
                            Font path
    -i FILENAME, --image FILENAME
                            Background image
    -n STRING, --name STRING
                            Mex name
    -o FILENAME, --output FILENAME
                            Filename of output image
    -s, --square          A switch to select padding values based on whether 1-1
                            or 9-5 hero image is being used. Alternatively,
                            specify the paddings.
    -x PADDINGX, --paddingx PADDINGX
                            Optionally, specify X padding in pixels
    -y PADDINGY, --paddingy PADDINGY
                            Optionally, specify Y padding in pixels
            
## Example

    ./mex_placeholder.py -f ../Sanomat/SanomatSans-Bold.otf -i ../MEX_Placeholder_95.jpg -n 'Mex Test' -o output.jpg
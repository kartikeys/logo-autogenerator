echo "------------Executing bars----------------" 
echo "Generating Logos" 
cd bars
mkdir imgs
python run_logo.py > failed_bars.txt
python s3_upload.py > failed_s3_bars.txt
rm -rf imgs/
cd ..

echo "------------Executing beauty----------------" 
echo "Generating Logos" 
cd beauty
mkdir imgs
python run_logo.py > failed_beauty.txt
python s3_upload.py > failed_s3_beauty.txt
rm -rf imgs/
cd ..

echo "------------Executing fashion----------------" 
echo "Generating Logos" 
cd fashion
mkdir imgs
python run_logo.py > failed_fashion.txt
python s3_upload.py > failed_s3_fashion.txt
rm -rf imgs/
cd ..

echo "------------Executing fun----------------" 
echo "Generating Logos" 
cd fun
mkdir imgs
python run_logo.py > failed_fun.txt
python s3_upload.py > failed_s3_fun.txt
rm -rf imgs/
cd ..

echo "------------Executing services----------------" 
echo "Generating Logos" 
cd services
mkdir imgs
python run_logo.py > failed_services.txt
python s3_upload.py > failed_s3_services.txt
rm -rf imgs/
cd ..

echo "------------Executing other_shops----------------" 
echo "Generating Logos" 
cd other_shops
mkdir imgs
python run_logo.py > failed_other_shops.txt
python s3_upload.py > failed_s3_other_shops.txt
rm -rf imgs/
cd ..

echo "------------Executing health----------------" 
echo "Generating Logos" 
cd health
mkdir imgs
python run_logo.py > failed_health.txt
python s3_upload.py > failed_s3_health.txt
rm -rf imgs/
cd ..

echo "------------Executing food_sg----------------" 
echo "Generating Logos" 
cd food_sg
mkdir imgs
python run_logo.py > failed_foodsg.txt
python s3_upload.py > failed_s3_foodsg.txt
rm -rf imgs/
cd ..

echo "------------Executing food_my----------------" 
echo "Generating Logos" 
cd food_my
mkdir imgs
python run_logo.py > failed_foodmy.txt
python s3_upload.py > failed_s3_foodmy.txt
rm -rf imgs/
cd ..

echo "=================DONE======================="
#!/usr/bin/env python
import csv
import commands
from tqdm import tqdm
from multiprocessing.pool import ThreadPool

reader = csv.reader(open('services.csv'))
cmd = []

for line in reader:
    grab_id = line[0]
    mexName = line[1]
    mexName = mexName.title()
    mexName = mexName.split(' -')[0]
    mexName = mexName.split(' @')[0]
    mexName = mexName.split(' |')[0]
    mexName = mexName.split(' (')[0]
    if len(mexName) > 30:
        continue
    cmd.append("python ../mex_placeholder.py -i ./Services.jpg -n "+'"{}"'.format(mexName)+" -o imgs/"+grab_id+".jpg -f 'Sanomat Grab Web Medium Regular.ttf' --height=800 --color='28,28,28'")

def generate_logos(c):
    status_code, err_msg = commands.getstatusoutput(c)
    if status_code != 0:
        return c, err_msg
    
    return c, None

for val in tqdm(cmd):
    msg, error = generate_logos(val)
    if error is not None:
        print("error fetching %r: %s" % (msg, error))

